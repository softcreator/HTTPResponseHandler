package com.salman.androidnetworking;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends Activity {

    Button btnPostData, btnGetData, btnImage;
    public static MainActivity instance = null;

    String postUrlLink = null;
    String getUrlLink = null;
    String postValue;
    String getValue;
    TextView outputView;
    ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        outputView = (TextView) findViewById(R.id.showOutput);
        btnPostData = (Button) findViewById(R.id.btnPostData);
        btnGetData = (Button) findViewById(R.id.btnGetData);
        btnImage = (Button) findViewById(R.id.btnImage);

        instance = this;

        postUrlLink = "http://en.prothom-alo.com/bangladesh/news/96625/37th-BCS-exams-circular-published";
        getUrlLink = "http://en.prothom-alo.com/bangladesh/news/96625/37th-BCS-exams-circular-published" ;

        btnPostData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HTTPResponseHandler postClass = new HTTPResponseHandler(MainActivity.this);
                postValue = postClass.outputFromPostMethod;
                outputView.setText(postValue);

            }
        });

        btnGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HTTPResponseHandler postClass = new HTTPResponseHandler(MainActivity.this);
                getValue = postClass.outputFromPostMethod;
                outputView.setText(getValue);

            }
        });

        // imageView button
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                HTTPResponseHandler downloadIm = new HTTPResponseHandler(MainActivity.this);
                HTTPResponseHandler.DownloadIm di = new HTTPResponseHandler.DownloadIm(MainActivity.this);



                ivImage = (ImageView) findViewById(R.id.iv_image);
                Bitmap img = di.outputImage;
                ivImage.setImageBitmap(img);

                Toast.makeText(MainActivity.this, "btnImage Button Clicked", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public static MainActivity getInstance() {
        return instance;
    }

    public void sendPostRequest(View View) {

        HashMap<String, String> hashMap = new HashMap<String, String>();


        hashMap.put("name", "sal");
        hashMap.put("email", "mail@gg.co");
        hashMap.put("pass", "***");
        hashMap.put("age", "22");
        hashMap.put("grade", "A+");

        HTTPResponseHandler postData=new HTTPResponseHandler(this,"POST", postUrlLink, hashMap);
        postData.execute();


    }

    public void sendGetRequest(View View) {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("name", "sal");
        hashMap.put("email", "mail@gg.com");

        HTTPResponseHandler postClass = new HTTPResponseHandler(this,"GET", postUrlLink, hashMap);
        postClass.execute();
    }


    public void downloadImage(View view) {

        HTTPResponseHandler.DownloadIm di = new HTTPResponseHandler.DownloadIm(this);
        di.execute("https://upload.wikimedia.org/wikipedia/commons/8/8c/JPEG_example_JPG_RIP_025.jpg");

    }


}
