package com.salman.androidnetworking;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aditya on 2/29/16.
 */
public class HTTPResponseHandler extends AsyncTask<String, Void , Void>{
    public static ProgressDialog progress;
    private final Context context;
    private ProgressDialog pDialog;

    public static String outputFromPostMethod;
    private HashMap<String, String> perameter;
    private String urlPerameter = "";
    private String urlLink;
    private String requestType="";
    public Bitmap outputImage;


    public HTTPResponseHandler(Context context) {
        this.context = context;
    }


    public HTTPResponseHandler(Context c, String urlLink) {
        this.urlLink = urlLink;
        this.context = c;
    }

    public HTTPResponseHandler(Context context,String requestType, String urlLink, HashMap<String, String> perameter) {
        this.urlLink = urlLink;
        this.context = context;
        this.perameter = perameter;
        this.requestType=requestType;
    }

    protected void onPreExecute() {
        progress = new ProgressDialog(this.context);
        progress.setMessage("Loading");
        progress.show();
    }
    protected void onPostExecute() {
        if(progress!=null){
            progress.dismiss();
        }

    }


    @Override
    protected Void doInBackground(String... params) {
        try {
            ArrayList<String> keys = new ArrayList<>();
            ArrayList<String> values = new ArrayList<>();

            //getting data from hashmap and put key and value to arraylist
            for (String key : perameter.keySet()) {
                keys.add(key);
            }

            for (String value : perameter.values()) {
                values.add(value);
            }
            //end getting data from hashmap

            //Formattiong data from arraylist
            for (int i = (keys.size() - 1); i >= 0; i--) {
                if (urlPerameter.equals("")) {
                    urlPerameter += keys.get(i) + "=" + values.get(i);
                } else {
                    urlPerameter += "&" + keys.get(i) + "=" + values.get(i);
                }
            }
            //end Formattiong data from arraylist

            URL url = new URL(urlLink);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            String urlParameters = urlPerameter;
            connection.setRequestMethod(requestType);  //Check
            connection.setDoOutput(true);
            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            dStream.writeBytes(urlParameters);
            dStream.flush();
            dStream.close();
            int responseCode = connection.getResponseCode();

            final StringBuilder output = new StringBuilder("Request URL " + url);
            output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
            output.append(System.getProperty("line.separator") + "Response Code " + responseCode);http:  //prothom-alo.com link
            output.append(System.getProperty("line.separator") + "Type " + "POST");
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = "";
            StringBuilder responseOutput = new StringBuilder();

            while ((line = br.readLine()) != null) {
                responseOutput.append(line);
            }

            br.close();

            output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

            progress.dismiss();

            outputFromPostMethod = output.toString();

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

        public static class DownloadIm extends AsyncTask<String, Integer, Bitmap> {
            public static Bitmap outputImage;

            Context context;

            public DownloadIm(Context c) {
                this.context = c;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress = new ProgressDialog(this.context);
                progress.setMessage("Downloading file. Please wait...");
                progress.setIndeterminate(true);
                progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progress.setCancelable(false);
                progress.setProgressNumberFormat("0 of 0");
                progress.show();
            }

            @Override
            protected Bitmap doInBackground(String... url) {

                int count;
                Bitmap bitmap = null;
                InputStream iStream = null;
                try {
                    URL mUrl = new URL(url[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) mUrl.openConnection();
                    urlConnection.connect();
                    iStream = urlConnection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(iStream);


                    int lenghtOfFile = urlConnection.getContentLength();
                    // input stream to read file - with 4k buffer
                    InputStream input = new BufferedInputStream(mUrl.openStream(), 4096);
                    // Output stream to write file
                    OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");
                    byte data[] = new byte[1024];
                    int values = 0;
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;

//                    int progress_temp = ((int) ((total * 100) / lenghtOfFile)); // 0 of 100
                        int progress_temp = ((int) (total /1000));
                        progress.setIndeterminate(false);
                        progress.setProgressNumberFormat("%1d kb of %2d kb");
                        progress.setMax(lenghtOfFile / 1000);

                        if (progress_temp % 10 == 0 && values != progress_temp) {
                            values = progress_temp;
                        }

                        publishProgress(progress_temp);

                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();
                    output.close();
                    input.close();

                } catch (Exception e) {
//                Log.d("Exception while downloading url", e.toString());
                } finally {
                }

                return bitmap;

            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                progress.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                //Save Image To Storage
                OutputStream fOut = null;
                Uri outputFileUri;
                outputImage = result;
                progress.dismiss();

                try {
                    File root = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "nn" + File.separator);
                    root.mkdirs();
                    File sdImageMainDirectory = new File(root, "myPicName.jpg");
                    outputFileUri = Uri.fromFile(sdImageMainDirectory);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                } catch (Exception e) {

                }
                try {
                    result.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                } catch (Exception e) {

                }
                // End Saving
                Toast.makeText(context, "Image downloaded successfully", Toast.LENGTH_SHORT).show();
            }


        }
    }


